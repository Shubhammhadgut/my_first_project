package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class MysqlConnect {

	public static void main(String[] args) {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
			conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/emp","root","smarte");
			
			Statement st=conn.createStatement();
			
			ResultSet rs=st.executeQuery("select * from details");
			
			System.out.println("ID"+" "+"Name"+" "+"Age");
			
			while(rs.next()) {
				System.out.println(rs.getInt(1)+" "+rs.getString(2)+" "+rs.getInt(3));
			}
			
			conn.close();
			
		} catch (Exception e) {
				e.printStackTrace();
		}

	}

}
